import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory } from 'react-router'
import App from './App';
import ProjectDetails from './js/components/ProjectDetails.js';

ReactDOM.render(
    (<Router history={hashHistory}>
        <Route component={App}>
            <Route path="/(:project)" component={ProjectDetails} />
        </Route>
    </Router>),
    document.getElementById('root'));
