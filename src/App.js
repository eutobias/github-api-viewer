import React, { Component } from 'react';
import './css/App.css';

import ProjectsList from './js/components/ProjectsList.js';

class App extends Component {
    render() {
        return (
            <div className="App">
                <ProjectsList />

                <main className="ProjectDetails-wrapper">
                    {this.props.children}
                </main>
            </div>
        );
    }
}

export default App;
