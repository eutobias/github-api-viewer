import Reflux from 'reflux';
import $ from 'jquery';

var ProjectsDetailsActions = Reflux.createActions({
    "loadRepos": {children: ["completed","failed"]},
    "loadCommits": {children: ["completed","failed"]},
    "getNextPage" : {children: ["completed","failed"]}
});
var pagination = {
    next : "",
    last : ""
}
ProjectsDetailsActions.loadRepos.listen(function ( url ) {
    var self = this;

    $.get(url, function(data) {

        // Load 20 first commits of project
        var url = data.commits_url.replace('{/sha}','') + '?per_page=20';
        ProjectsDetailsActions.loadCommits(url)

        self.completed(data);
    })
    .fail(function () {
        self.failed("Erro de conexão")
    });
});

ProjectsDetailsActions.loadCommits.listen(function ( url ) {
    var self = this;

    $.get(url, function(data, status, headers) {
        // console.log('Loading initial commits', data.length);
        var hasNextPage = data.length >= 20;
        if ( hasNextPage )
        {
            var links = headers.getResponseHeader('Link').replace(/</g,'').replace(/>/g,"").split(',');
            pagination.next = links[0].split(";")[0];
            pagination.last = links[1].split(";")[0];
        }

        self.completed(data, hasNextPage);
    })
    .fail(function () {
        self.failed("Erro de conexão")
    });
});

ProjectsDetailsActions.getNextPage.listen(function () {
    var self = this;

    $.get(pagination.next, function(data, status, headers) {
        // console.log('Loading more commits', data.length);

        var links = headers.getResponseHeader('Link').replace(/</g,'').replace(/>/g,"").split(',');

        pagination.next = links[0].split(";")[0];
        pagination.last = links[1].split(";")[0];

        var hasNextPage = pagination.next.substr(  pagination.next.length-6,  pagination.next.length ) !== 'page=1';

        self.completed(data, hasNextPage);
    })
    .fail(function () {
        self.failed("Erro de conexão")
    });

});

module.exports = ProjectsDetailsActions;
