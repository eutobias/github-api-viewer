import Reflux from 'reflux';
import $ from 'jquery';

const GET_ALL_REPOS_URL = "https://api.github.com/users/globocom/repos";

var ProjectsListActions = Reflux.createActions({
    "load": {children: ["completed","failed"]}
});

ProjectsListActions.load.listen(function () {
    var self = this;

    $.get(GET_ALL_REPOS_URL, function(data) {

        //Sort by stargazers_count desc
        data.sort((a,b) => {
            if (a.stargazers_count < b.stargazers_count)
              return -1;
            if (a.stargazers_count > b.stargazers_count)
              return 1;
            return 0;
        }).reverse();

        //Load first project info
        // ProjectsDetailsActions.loadRepos(data[0].url);

        self.completed(data);

    })
    .fail(function () {
        self.failed("Erro de conexão")
    });
});

module.exports = ProjectsListActions;
