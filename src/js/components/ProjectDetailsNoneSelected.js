import React from 'react';

class ProjectDetailsNoneSelected extends React.Component {

    render() {
        return (
            <div className="ProjectDetails-content-wrapper">
                <h2 className="ProjectDetails-waiting-selection">Escolha um projeto no menu.</h2>
            </div>
        );
    }
}

export default ProjectDetailsNoneSelected;
