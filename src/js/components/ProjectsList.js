import React from 'react';
import Reflux from 'reflux';
import ProjectsListStore from '../store/ProjectsListStore.js'
import ProjectsListItem from './ProjectsListItem.js'
import ProjectsListActions from '../actions/ProjectsListActions.js';

class ProjectsList extends Reflux.Component {
    constructor (props) {
        super(props)
        this.state = {};
        this.store = ProjectsListStore;
    }
    componentDidMount () {
        ProjectsListActions.load();
    }
    render() {
        return (
            <nav className="ProjectList-wrapper">
                <a id="open-mobile-menu" className="visible" href='#'><i className="fa fa-bars"></i></a>
                <a id="close-mobile-menu" href='#'><i className="fa fa-times"></i></a>

                <div className="ProjectList-content-wrapper">
                    <h1 className="ProjectList-title">Projetos</h1>
                    <hr className="ProjectList-divider" />
                    <ul className="ProjectList-list">
                        {this.store.state.projects.map((o,i) => {
                            return <ProjectsListItem key={i} id={i} item={o} />
                        } )}
                    </ul>
                </div>
            </nav>
        );
    }
}

export default ProjectsList;
