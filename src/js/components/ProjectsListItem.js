import React, { Component } from 'react';
import { Link } from 'react-router';

import ProjectsDetailsActions from '../actions/ProjectsDetailsActions.js';

class ProjectsList extends Component {

    constructor(props) {
        super(props)
        this.props = props;

    }

	handleLoadProjectData() {
        ProjectsDetailsActions.loadRepos(this.props.item.url);
	}

    render() {
        return (
            <li className="ProjectList-item">
                <Link to={ this.props.item.name + ":" + this.props.item.description } activeClassName="active" onClick={this.handleLoadProjectData.bind(this)}>
                    {this.props.item.name}
                </Link>
            </li>
        );
    }
}

export default ProjectsList;
