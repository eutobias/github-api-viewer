import React from 'react';
import Reflux from 'reflux';

import ProjectsDetailsStore from '../store/ProjectsDetailsStore.js';
import ProjectDetailsNoneSelected from './ProjectDetailsNoneSelected.js';
import ProjectDetailsSelected from './ProjectDetailsSelected.js';

class ProjectsList extends Reflux.Component {

    constructor (props) {
        super(props)
        this.store = ProjectsDetailsStore;
    }

    render() {
        return (
            this.state.project.id ? <ProjectDetailsSelected data={this.state} /> : <ProjectDetailsNoneSelected />
        );
    }
}

export default ProjectsList;
