import React from 'react';
import Reflux from 'reflux';

import ProjectsDetailsActions from '../actions/ProjectsDetailsActions.js';

class ProjectsList extends Reflux.Component {

    handleGetNextPage() {
        ProjectsDetailsActions.getNextPage();
    }

    render() {
        return (
            <div className="ProjectDetails-content-wrapper">
                <h3 className="ProjectDetails-title">{this.props.data.project.name}</h3>
                <hr className="ProjectDetails-divider" />
                <div className="ProjectDetails-description">{this.props.data.project.description}</div>
                <ul className="ProjectDetails-stats">
                    <li><i className="fa fa-star"></i> {this.props.data.project.stargazers_count}</li>
                    <li><i className="fa fa-code-fork"></i> {this.props.data.project.forks}</li>
                </ul>

                <ul className="ProjectDetails-commit-list">
                    {this.props.data.commits.map((o,i) => {
                        return (
                            <li key={o.sha}>
                                <span className="ProjectDetails-commit-text">{o.commit.message}</span>
                                <span className="ProjectDetails-commit-hash">{o.sha.substr(0,7)}</span>
                            </li>
                        )
                    })}
                </ul>

                <div className="ProjectDetails-load-more">
                    {this.props.data.hasNextPage ? <a onClick={this.handleGetNextPage.bind(this)}>Mais commits...</a> : null }
                </div>
            </div>
        );
    }
}

export default ProjectsList;
