import Reflux from 'reflux';
import ProjectsDetailsActions from '../actions/ProjectsDetailsActions.js';

var ProjectsDetailsStore = Reflux.createStore({
    listenables: ProjectsDetailsActions,

    init: function() {
        this.state = {project : {}, commits: [], hasNextPage:true};
    },

    getInitialState() {
        return this.state;
    },

    onLoadReposCompleted: function(result) {
        this.state.project = result;
        this.trigger({ project : result});
    },
    onLoadReposProjectsFailed: function(err) {
        alert(err);
    },

    onLoadCommitsCompleted: function(result, hasNextPage) {
        // this.state.all_commits = result;
        this.state.commits = result;
        this.state.hasNextPage = hasNextPage;
        this.trigger({ commits : this.state.commits, hasNextPage : this.state.hasNextPage });
    },
    onLoadCommitsProjectsFailed: function(err) {
        alert(err);
    },

    onGetNextPageCompleted: function(result, hasNextPage) {

        var len = result.length;
        for (var i=0; i<len; i++)
        {
            this.state.commits.push(result[i]);
        }

        this.state.hasNextPage = hasNextPage;

        this.trigger({ commits : this.state.commits, hasNextPage : this.state.hasNextPage });
    },
    onGetNextPageFailed: function(err) {
        alert(err);
    }
});

module.exports = ProjectsDetailsStore;
