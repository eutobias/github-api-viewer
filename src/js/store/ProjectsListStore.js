import Reflux from 'reflux';
import ProjectsListActions from '../actions/ProjectsListActions.js';

var ProjectsListStore = Reflux.createStore({
    listenables: ProjectsListActions,

    init: function() {
        this.state = {projects : []};
    },

    getInitialState() {
        return this.state;
    },

    onLoadCompleted: function(result) {
        this.state.projects = result;
        this.trigger({ projects : result});
    },
    onLoadProjectsFailed: function(err) {
        alert(err);
    }
});

module.exports = ProjectsListStore;
